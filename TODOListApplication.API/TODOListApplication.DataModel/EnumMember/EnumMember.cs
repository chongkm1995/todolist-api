﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TODOListApplication.DataModel.EnumMember
{
    public enum TaskStatus
    { 
        NotStarted = 0,
        InProgress = 1,
        Completed = 2
    }

    public enum TaskPriority
    { 
        Low = 0,
        Medium = 1,
        High = 2
    }

    public enum ErrorCode
    { 
        Success = 0,
        InternalServerError = -500,
        
        FailedToCreateTask = -100,
        TaskNameRequired = -101,
        InvalidDueDate = -102,
        InvalidStatus = -103,
        InvalidPriority = -104,
        FailedToUpdateTask = -105,
        FailedToDeleteTask = -106,
        TaskNotFound = -107
    }

    public static class ErrorCodeExtension
    { 
        public static string ToMeaningfulMessage(this ErrorCode code) 
        { 
            switch (code) 
            {
                case ErrorCode.Success:
                    return "Operation Success";                
                case ErrorCode.FailedToCreateTask:
                    return "Failed to create task";
                case ErrorCode.TaskNameRequired:
                    return "Task name cannot empty";
                case ErrorCode.InvalidDueDate:
                    return "Invalid due date";
                case ErrorCode.InvalidStatus:
                    return "Invalid status";
                case ErrorCode.InvalidPriority:
                    return "Invalid priority";
                case ErrorCode.FailedToUpdateTask:
                    return "Failed to updae task";
                case ErrorCode.FailedToDeleteTask:
                    return "Failed to delete task";
                case ErrorCode.TaskNotFound:
                    return "Task not found";
                case ErrorCode.InternalServerError:
                default:
                    return "Internal Server Error";
            }
        }
    }
}
