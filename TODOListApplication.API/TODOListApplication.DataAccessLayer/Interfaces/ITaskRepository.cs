﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TODOListApplication.DataAccessLayer.Interfaces
{
    public interface ITaskRepository
    {
        int Create(DataModel.Task task);

        IEnumerable<DataModel.Task> GetAll();

        bool Update(DataModel.Task task);

        bool Delete(int taskId);
    }
}
