﻿namespace TODOListApplication.API.Models
{
    public class CreateTaskAPIRequest
    {
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string DueDate { get; set; } = string.Empty;
        public string Tags { get; set; } = string.Empty;
        public int Status { get; set; }
        public int Priority { get; set; }
    }
}
