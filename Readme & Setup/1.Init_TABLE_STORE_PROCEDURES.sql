USE [TODOListDB]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 26/3/2024 11:15:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[DueDate] [datetime] NULL,
	[Tags] [nvarchar](500) NULL,
	[Status] [smallint] NULL,
	[Priority] [smallint] NULL,
	[CreatedDateUTC] [datetime] NULL,
	[UpdatedDateUTC] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Task_Create]    Script Date: 26/3/2024 11:15:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Task_Create]
	@Name NVARCHAR(100),
	@Description NVARCHAR(500),
	@DueDate DATETIME,
	@Tags NVARCHAR(500),
	@Status SMALLINT,
	@Priority SMALLINT,
	@Id INT OUTPUT
AS
BEGIN    
	INSERT INTO Tasks 
	(
		[Name], 
		[Description], 
		[DueDate], 
		[Tags], 
		[Status], 
		[Priority], 
		[CreatedDateUTC]
	)
	VALUES 
	(
		@Name,
		@Description,
		@DueDate,
		@Tags,
		@Status,
		@Priority,
		GETUTCDATE()
	)

	SET @Id = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[Task_Delete]    Script Date: 26/3/2024 11:15:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Task_Delete]
	@Id INT
AS
BEGIN
	DELETE FROM Tasks WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Task_GetAll]    Script Date: 26/3/2024 11:15:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Task_GetAll]
	
AS
BEGIN
	SET NOCOUNT ON;

    SELECT * FROM Tasks
END
GO
/****** Object:  StoredProcedure [dbo].[Task_Update]    Script Date: 26/3/2024 11:15:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Task_Update]
	@Id INT,
	@Name NVARCHAR(100),
	@Description NVARCHAR(500),
	@DueDate DATETIME,
	@Tags NVARCHAR(500),
	@Status SMALLINT,
	@Priority SMALLINT
AS
BEGIN
    UPDATE a SET
		[Name] = @Name,
		[Description] = @Description,
		[DueDate] = @DueDate,
		[Tags] = @Tags,
		[Status] = @Status,
		[Priority] = @Priority
	FROM Tasks a 
	WHERE Id = @Id
END
GO
