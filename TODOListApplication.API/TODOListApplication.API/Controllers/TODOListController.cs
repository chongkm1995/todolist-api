﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using TODOListApplication.API.Models;
using TODOListApplication.ApplicationLayer;
using TODOListApplication.ApplicationLayer.Interfaces;

namespace TODOListApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TODOListController : ControllerBase
    {
        readonly ITODOListApplicationService _appService;

        public TODOListController() 
        {
            _appService = GlobalContext.GetTodoAppService;
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create(CreateTaskAPIRequest request) 
        {
            if(request == null) return Response(-1, "Invalid Parameter");

            var result = _appService.Create(new DataModel.Task 
            { 
                Name = request.Name,
                Description = request.Description,
                DueDate = request.DueDate,
                Tags = request.Tags,
                Status = request.Status,
                Priority = request.Priority,
            });

            return Response(result.Item1, result.Item2);
        }

        [HttpGet]
        [Route("getall")]
        public IActionResult GetAll() 
        {
            var result = _appService.GetAll();

            return Response(result.Item1, result.Item2, result.Item3);
        }

        [HttpGet]
        [Route("get")]
        public IActionResult Get(int taskId) 
        {
            var result = _appService.Get(taskId);

            return Response(result.Item1, result.Item2, result.Item3);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update(UpdateTaskAPIRequest request) 
        {
            if (request == null) return Response(-1, "Invalid Parameter");

            var result = _appService.Update(new DataModel.Task
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                DueDate = request.DueDate,
                Priority = request.Priority,
                Status = request.Status,
                Tags = request.Tags,
            });

            return Response(result.Item1, result.Item2);
        }

        [HttpDelete]
        [Route("delete")]
        public IActionResult Delete(int taskId) 
        {
            var result = _appService.Delete(taskId);

            return Response(result.Item1, result.Item2);
        }        

        private new IActionResult Response(int error_code, string error_message, object data = null) 
        {
            return Content(JsonConvert.SerializeObject(new
            {
                ErrorCode = error_code,
                ErrorMessage = error_message,
                Result = data
            }), "application/json", Encoding.UTF8);
        }
    }
}
