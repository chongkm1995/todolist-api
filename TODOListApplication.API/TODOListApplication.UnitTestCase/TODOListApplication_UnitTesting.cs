using Microsoft.Extensions.Configuration;
using Microsoft.Graph;
using Microsoft.Graph.Models;
using System.Reflection.Metadata.Ecma335;
using TODOListApplication.ApplicationLayer;
using TODOListApplication.ApplicationLayer.Interfaces;
using TODOListApplication.DataAccessLayer;
using TODOListApplication.DataAccessLayer.Interfaces;
using TODOListApplication.DataModel.EnumMember;
using TODOListApplication.ServiceLayer;
using TODOListApplication.ServiceLayer.Interfaces;
using Xunit.Sdk;
using TaskStatus = TODOListApplication.DataModel.EnumMember.TaskStatus;

namespace TODOListApplication.UnitTestCase
{
    [TestClass]
    public class TODOListApplication_UnitTesting
    {
        ITaskService _taskService;
        ITaskRepository _taskRepo;
        ITODOListApplicationService _todoAppService;

        IConfiguration _configuration;

        [TestInitialize]
        public void Init()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory.Replace(@"\bin\Debug\net6.0", "");

            var builder = new ConfigurationBuilder()
                .SetBasePath(path)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true);

            _configuration = builder.Build();

            _taskRepo = new TaskRepository(_configuration);
            _taskService = new TaskService(_configuration, _taskRepo);
            _todoAppService = new TODOListApplicationService(_configuration, _taskService);

            Assert.IsNotNull(_taskRepo);
            Assert.IsNotNull(_taskService);
            Assert.IsNotNull(_todoAppService);
        }

        DataModel.Task MockTask()
        {
            return new DataModel.Task
            {
                Name = "Integrate payment gateway API",
                Description = "Detail will be provide soon",
                DueDate = DateTime.Now.AddDays(50).Date.ToString("yyyy-MM-dd"),
                Tags = "New Task, Request by client",
                Priority = (int)TaskPriority.Low,
                Status = (int)TaskStatus.NotStarted
            };
        }

        #region Create Task

        [TestMethod]
        public void CreateTaskFailed_EmptyTaskName()
        {
            var task = MockTask();
            task.Name = string.Empty;

            var result = _todoAppService.Create(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.TaskNameRequired);
        }

        [TestMethod]
        public void CreateTaskFailed_InvalidDueDate()
        {
            var task = MockTask();
            task.DueDate = "abc";

            var result = _todoAppService.Create(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.InvalidDueDate);
        }

        [TestMethod]
        public void CreateTaskFailed_InvalidStatus() 
        {
            var task = MockTask();
            task.Status = -100;

            var result = _todoAppService.Create(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.InvalidStatus);
        }

        [TestMethod]
        public void CreateTaskFailed_InvalidPriority()
        {
            var task = MockTask();
            task.Priority = -100;

            var result = _todoAppService.Create(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.InvalidPriority);
        }

        [TestMethod]
        public void CreateTaskSuccessfully() 
        {
            var task = MockTask();

            var result = _todoAppService.Create(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.Success);
        }

        #endregion

        #region Get all tasks

        [TestMethod]
        public void GetAllTasks() 
        {
            var result = _todoAppService.GetAll();

            Assert.IsTrue(result.Item1 == (int)ErrorCode.Success);
            Assert.IsTrue(result.Item3.Count() > 0);
        }

        #endregion

        #region Get Single Task

        [TestMethod]
        public void GetSingleTaskById() 
        {
            var result = _todoAppService.Get(1);

            Assert.IsTrue(result.Item1 == (int)result.Item1);
            Assert.IsNotNull(result.Item3);
            Assert.IsTrue(result.Item3.Id > 0);
        }

        #endregion

        #region Update Task

        [TestMethod]
        public void UpdateTaskFailed_TaskNotFound() 
        {
            var task = GetTask();

            if (task == null) throw new AssertFailedException();

            task.Id = 100;
            var result = _todoAppService.Update(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.TaskNotFound);
        }

        [TestMethod]
        public void UpdateTaskFailed_TaskNameEmpty() 
        {
            var task = GetTask();

            if (task == null) throw new AssertFailedException();

            task.Name = string.Empty;
            var result = _todoAppService.Update(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.TaskNameRequired);
        }

        [TestMethod]
        public void UpdateTaskFailed_InvalidDueDate() 
        {
            var task = GetTask();

            if(task == null) throw new AssertFailedException();

            task.DueDate = "abcdef";
            var result = _todoAppService.Update(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.InvalidDueDate);
        }

        [TestMethod]
        public void UpdateTaskFailed_InvalidPriority() 
        {
            var task = GetTask();

            if (task == null) throw new AssertFailedException();

            task.Priority = -100;
            var result = _todoAppService.Update(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.InvalidPriority);
        }

        [TestMethod]
        public void UpdateTaskFailed_InvalidStatus()
        {
            var task = GetTask();

            if (task == null) throw new AssertFailedException();

            task.Status = -100;
            var result = _todoAppService.Update(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.InvalidStatus);
        }

        [TestMethod]
        public void UpdateTaskSuccessfully()
        {
            var task = GetTask();

            if (task == null) throw new AssertFailedException();

            task.Name = "Integrate payment gateway API for AirAsia";
            task.Description = "Implement for big client AirAsia";
            task.Priority = (int)TaskPriority.High;
            task.Tags = "High priority, airasia";
            task.Status = (int)TaskStatus.InProgress;

            var result = _todoAppService.Update(task);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.Success);
        }

        #endregion

        #region Delete Task

        [TestMethod]
        public void DeleteTaskFailed_TaskNotFound() 
        {
            var result = _todoAppService.Delete(100);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.TaskNotFound);
        }

        [TestMethod]
        public void DeleteTaskSuccessfully() 
        {
            var result = _todoAppService.Delete(1);

            Assert.IsTrue(result.Item1 == (int)ErrorCode.Success);
        }

        #endregion 

        DataModel.Task GetTask() 
        {
            var getTaskResult = _todoAppService.Get(1);

            return getTaskResult.Item3;
        }
    }
}