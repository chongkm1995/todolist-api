﻿using System;
using System.Collections.Generic;
using System.Linq;
using TODOListApplication.DataModel;

namespace TODOListApplication.ApplicationLayer.Interfaces
{
    public interface ITODOListApplicationService
    {
        Tuple<int, string> Create(DataModel.Task task);

        Tuple<int, string, IEnumerable<DataModel.Task>> GetAll();

        Tuple<int, string, DataModel.Task> Get(int id);

        Tuple<int, string> Update(DataModel.Task task);

        Tuple<int, string> Delete(int id);
    }
}
