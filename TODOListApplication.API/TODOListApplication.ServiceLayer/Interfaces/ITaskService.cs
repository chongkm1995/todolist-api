﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TODOListApplication.ServiceLayer.Interfaces
{
    public interface ITaskService
    {
        int Create(DataModel.Task task);

        IEnumerable<DataModel.Task> GetAll();

        DataModel.Task Get(int taskId);

        bool IsExist(int taskId);

        int Update(DataModel.Task task);

        int Delete(int taskId);
    }
}
