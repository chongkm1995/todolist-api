﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;
using System.Reflection;
using TODOListApplication.ApplicationLayer.Interfaces;
using TODOListApplication.DataAccessLayer.Interfaces;
using TODOListApplication.DataModel.EnumMember;
using TODOListApplication.ServiceLayer;
using TODOListApplication.ServiceLayer.Interfaces;

namespace TODOListApplication.ApplicationLayer
{    
    public class TODOListApplicationService : ITODOListApplicationService
    {
        readonly ITaskService _service;
        readonly ErrorCode _internalServerError = ErrorCode.InternalServerError;
        readonly string _internalServerErrorMessage;

        readonly ILogger _log;

        public TODOListApplicationService(
            IConfiguration config, 
            ITaskService service)
        {
            _service = service;
            _internalServerErrorMessage = _internalServerError.ToMeaningfulMessage();

            _log = LogManager.GetCurrentClassLogger();
        }

        public Tuple<int, string> Create(DataModel.Task task)
        {
            Tuple<int, string> result = null;

            var refId = Guid.NewGuid();
            var methodName = MethodBase.GetCurrentMethod()?.Name;

            _log.Info(JsonConvert.SerializeObject(new 
            { 
                RefId = refId,
                Method = methodName,
                Request = task
            }));

            try
            {
                var result_info = InternalCreate(task);

                result = new Tuple<int, string>((int)result_info, result_info.ToMeaningfulMessage());
            }
            catch (Exception ex)
            {
                _log.Info(JsonConvert.SerializeObject(new 
                { 
                    RefId = refId,
                    Method = methodName,
                    Exception = ex
                }));

                result = new Tuple<int, string>((int)_internalServerError, _internalServerErrorMessage);
            }
            finally
            {
                if (result != null && result.Item1 != (int)_internalServerError)
                {
                    _log.Info(JsonConvert.SerializeObject(new
                    {
                        RefId = refId,
                        Method = methodName,
                        Response = result
                    }));
                }
            }

            return result;
        }

        public Tuple<int, string, IEnumerable<DataModel.Task>> GetAll()
        {
            Tuple<int, string, IEnumerable<DataModel.Task>> result = null;

            var refId = Guid.NewGuid();
            var methodName = MethodBase.GetCurrentMethod()?.Name;

            _log.Info(JsonConvert.SerializeObject(new
            {
                RefId = refId,
                Method = methodName                
            }));

            try
            {
                var result_info = InternalGetAll();

                result = new Tuple<int, string, IEnumerable<DataModel.Task>>((int)result_info.Item1, result_info.Item1.ToMeaningfulMessage(), result_info.Item2);
            }
            catch (Exception ex)
            {
                _log.Info(JsonConvert.SerializeObject(new
                {
                    RefId = refId,
                    Method = methodName,
                    Exception = ex
                }));

                result = new Tuple<int, string, IEnumerable<DataModel.Task>>((int)_internalServerError, _internalServerErrorMessage, new List<DataModel.Task>());
            }
            finally
            {
                if (result != null && result.Item1 != (int)_internalServerError) 
                {
                    _log.Info(JsonConvert.SerializeObject(new
                    {
                        RefId = refId,
                        Method = methodName,
                        Response = result
                    }));
                }
            }

            return result;
        }

        public Tuple<int, string, DataModel.Task> Get(int id)
        {
            Tuple<int, string, DataModel.Task> result = null;

            var refId = Guid.NewGuid();
            var methodName = MethodBase.GetCurrentMethod()?.Name;

            _log.Info(JsonConvert.SerializeObject(new
            {
                RefId = refId,
                Method = methodName,
                TaskId = id
            }));

            try
            {
                var result_info = InternalGet(id);

                result = new Tuple<int, string, DataModel.Task>((int)result_info.Item1, result_info.Item1.ToMeaningfulMessage(), result_info.Item2);
            }
            catch (Exception ex)
            {
                _log.Info(JsonConvert.SerializeObject(new
                {
                    RefId = refId,
                    Method = methodName,
                    Exception = ex
                }));

                result = new Tuple<int, string, DataModel.Task>((int)_internalServerError, _internalServerErrorMessage, new DataModel.Task());
            }
            finally
            {
                if (result != null && result.Item1 != (int)_internalServerError) 
                {
                    _log.Info(JsonConvert.SerializeObject(new
                    {
                        RefId = refId,
                        Method = methodName,
                        Response = result
                    }));
                }
            }

            return result;
        }                
        
        public Tuple<int, string> Update(DataModel.Task task)
        {
            Tuple<int, string> result = null;

            var refId = Guid.NewGuid();
            var methodName = MethodBase.GetCurrentMethod()?.Name;

            _log.Info(JsonConvert.SerializeObject(new
            {
                RefId = refId,
                Method = methodName,
                Request = task
            }));

            try
            {
                var result_info = InternalUpdate(task);

                result = new Tuple<int, string>((int)result_info, result_info.ToMeaningfulMessage());
            }
            catch (Exception ex)
            {
                _log.Info(JsonConvert.SerializeObject(new
                {
                    RefId = refId,
                    Method = methodName,
                    Exception = ex
                }));

                result = new Tuple<int, string>((int)_internalServerError, _internalServerErrorMessage);
            }
            finally
            {
                if (result != null && result.Item1 != (int)_internalServerError)
                {
                    _log.Info(JsonConvert.SerializeObject(new
                    {
                        RefId = refId,
                        Method = methodName,
                        Response = result
                    }));
                }
            }

            return result;
        }

        public Tuple<int, string> Delete(int id)
        {
            Tuple<int, string> result = null;

            var refId = Guid.NewGuid();
            var methodName = MethodBase.GetCurrentMethod()?.Name;

            _log.Info(JsonConvert.SerializeObject(new
            {
                RefId = refId,
                Method = methodName,
                TaskId = id
            }));

            try
            {
                var result_info = InternalDelete(id);

                result = new Tuple<int, string>((int)result_info, result_info.ToMeaningfulMessage());
            }
            catch (Exception ex)
            {
                _log.Info(JsonConvert.SerializeObject(new
                {
                    RefId = refId,
                    Method = methodName,
                    Exception = ex
                }));

                result = new Tuple<int, string>((int)_internalServerError, _internalServerErrorMessage);
            }
            finally
            {
                if (result != null && result.Item1 != (int)_internalServerError)
                {
                    _log.Info(JsonConvert.SerializeObject(new
                    {
                        RefId = refId,
                        Method = methodName,
                        Response = result
                    }));
                }
            }

            return result;
        }

        private ErrorCode InternalCreate(DataModel.Task task) 
        {
            var error_code = InternalValidateTaskParam(task);

            if (error_code != ErrorCode.Success) return error_code;

            var result = _service.Create(task);

            if (result < 0) return ErrorCode.FailedToCreateTask;

            return ErrorCode.Success;
        }

        private Tuple<ErrorCode, IEnumerable<DataModel.Task>> InternalGetAll()
        {
            var tasks = _service.GetAll();

            return new Tuple<ErrorCode, IEnumerable<DataModel.Task>>(ErrorCode.Success, tasks);
        }

        private Tuple<ErrorCode, DataModel.Task> InternalGet(int id) 
        { 
            var task = _service.Get(id);

            return new Tuple<ErrorCode, DataModel.Task>(ErrorCode.Success, task);
        }

        private ErrorCode InternalUpdate(DataModel.Task task) 
        {
            if (!_service.IsExist(task.Id)) return ErrorCode.TaskNotFound;

            var error_code = InternalValidateTaskParam(task);

            if (error_code != ErrorCode.Success) return error_code;

            var result = _service.Update(task);

            if (result < 0) return ErrorCode.FailedToUpdateTask;

            return ErrorCode.Success;
        }

        private ErrorCode InternalDelete(int id) 
        {
            if (!_service.IsExist(id)) return ErrorCode.TaskNotFound;

            var result = _service.Delete(id);

            if(result < 0) return ErrorCode.FailedToDeleteTask;

            return ErrorCode.Success;
        }

        private ErrorCode InternalValidateTaskParam(DataModel.Task task) 
        {
            if (string.IsNullOrEmpty(task.Name)) return ErrorCode.TaskNameRequired;

            DateTime dueDate;
            if (!DateTime.TryParse(task.DueDate, out dueDate) || dueDate < DateTime.Now) return ErrorCode.InvalidDueDate;

            if (!Enum.IsDefined(typeof(TaskPriority), task.Priority)) return ErrorCode.InvalidPriority;

            if (!Enum.IsDefined(typeof(DataModel.EnumMember.TaskStatus), task.Status)) return ErrorCode.InvalidStatus;

            return ErrorCode.Success;
        }
    }
}