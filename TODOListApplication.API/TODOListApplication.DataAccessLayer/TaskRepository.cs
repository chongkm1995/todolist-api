﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using TODOListApplication.DataAccessLayer.Interfaces;
using TODOListApplication.DataModel.EnumMember;

namespace TODOListApplication.DataAccessLayer
{
    public class TaskRepository : ITaskRepository
    {
        readonly IConfiguration _config;
        readonly string? _connectionString;

        public TaskRepository(IConfiguration config) 
        {
            _config = config;
            _connectionString = _config.GetConnectionString("default");
        }

        public int Create(DataModel.Task task)
        {
            using (var connection = new SqlConnection(_connectionString)) 
            {
                connection.Open();
                using (var command = connection.CreateCommand()) 
                {
                    command.CommandText = "Task_Create";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Name", task.Name);
                    command.Parameters.AddWithValue("@Description", task.Description);
                    command.Parameters.AddWithValue("@DueDate", task.DueDate);
                    command.Parameters.AddWithValue("@Tags", task.Tags);
                    command.Parameters.AddWithValue("@Priority", (int)task.Priority);
                    command.Parameters.AddWithValue("@Status", (int)task.Status);

                    var output_idParam = command.Parameters.Add("@Id", SqlDbType.Int);
                    output_idParam.Direction = ParameterDirection.Output;

                    var affected_row = command.ExecuteNonQuery();

                    if (affected_row > 0) 
                    {
                        var inserted_id = output_idParam.Value.ToString();

                        return Convert.ToInt32(inserted_id);
                    }

                    return -1;
                }
            }            
        }

        public IEnumerable<DataModel.Task> GetAll()
        {
            using (var connection = new SqlConnection(_connectionString)) 
            {
                connection.Open();
                using(var command = connection.CreateCommand()) 
                {
                    command.CommandText = "Task_GetAll";
                    command.CommandType = CommandType.StoredProcedure;

                    var reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            yield return InternalRead(reader);
                        }
                    }                    
                }
            }
        }

        public bool Update(DataModel.Task task)
        {
            using (var connection = new SqlConnection(_connectionString)) 
            {
                connection.Open();
                using (var command = connection.CreateCommand()) 
                {
                    command.CommandText = "Task_Update";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Id", task.Id);
                    command.Parameters.AddWithValue("@Name", task.Name);
                    command.Parameters.AddWithValue("@Description", task.Description);
                    command.Parameters.AddWithValue("@DueDate", task.DueDate);
                    command.Parameters.AddWithValue("@Tags", task.Tags);
                    command.Parameters.AddWithValue("@Priority", task.Priority);
                    command.Parameters.AddWithValue("@Status", task.Status);

                    var affected_row = command.ExecuteNonQuery();

                    return affected_row > 0;
                }
            }
        }

        public bool Delete(int taskId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using(var command = connection.CreateCommand()) 
                {
                    command.CommandText = "Task_Delete";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Id", taskId);

                    var affected_row = command.ExecuteNonQuery();

                    return affected_row > 0;
                }
            }
        }

        DataModel.Task InternalRead(SqlDataReader reader) 
        {
            return new DataModel.Task
            {
                Id = reader.GetInt32("Id"),
                Name = reader.GetString("Name"),
                Description = reader.GetString("Description"),
                DueDate = reader.GetDateTime("DueDate").ToString("yyyy-MM-dd HH:mm:ss"),
                Tags = reader.GetString("Tags"),
                Priority = reader.GetInt16("Priority"),
                Status = reader.GetInt16("Status")
            };
        }
    }
}