﻿using Microsoft.Extensions.Configuration;
using System.Collections.Concurrent;
using TODOListApplication.DataAccessLayer;
using TODOListApplication.DataAccessLayer.Interfaces;
using TODOListApplication.ServiceLayer.Interfaces;

namespace TODOListApplication.ServiceLayer
{
    public class TaskService : ITaskService
    {
        readonly ITaskRepository _taskRepo;
        readonly IDictionary<int, DataModel.Task> _tasksList;        

        public TaskService(
            IConfiguration config, 
            ITaskRepository taskRepo)
        {
            _taskRepo = taskRepo;

            _tasksList = new ConcurrentDictionary<int, DataModel.Task>();

            LoadTasks();
        }

        void LoadTasks()
        {
            var tasks = _taskRepo.GetAll();

            foreach (var task in tasks) 
            {
                _tasksList[task.Id] = task;
            }
        }

        public int Create(DataModel.Task task)
        {
            var id = _taskRepo.Create(task);

            if (id < 0) return -1;

            task.Id = id;

            _tasksList[id] = task;

            return 0;
        }

        public DataModel.Task Get(int taskId)
        {
            DataModel.Task task;
            if (_tasksList.TryGetValue(taskId, out task)) return task;

            return null;
        }

        public bool IsExist(int taskId)
        {
            return _tasksList.TryGetValue(taskId, out _);
        }

        public IEnumerable<DataModel.Task> GetAll()
        {
            return _tasksList.Values;
        }

        public int Update(DataModel.Task task)
        {
            var ok = _taskRepo.Update(task);

            if(!ok) return -1;

            _tasksList[task.Id] = task;

            return 0;
        }

        public int Delete(int taskId)
        {
            var ok = _taskRepo.Delete(taskId);

            if (!ok) return -1;

            _tasksList.Remove(taskId);

            return 0;
        }
    }
}