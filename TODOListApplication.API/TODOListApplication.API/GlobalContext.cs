﻿using TODOListApplication.ApplicationLayer;
using TODOListApplication.ApplicationLayer.Interfaces;
using TODOListApplication.DataAccessLayer;
using TODOListApplication.DataAccessLayer.Interfaces;
using TODOListApplication.ServiceLayer;
using TODOListApplication.ServiceLayer.Interfaces;

namespace TODOListApplication.API
{
    public static class GlobalContext
    {
        static ITaskRepository _taskRepo;
        static ITaskService _taskService;
        static ITODOListApplicationService _todoAppService;

        public static void Init(IConfiguration configuration) 
        {
            _taskRepo = new TaskRepository(configuration);
            _taskService = new TaskService(configuration, _taskRepo);
            _todoAppService = new TODOListApplicationService(configuration, _taskService);
        }

        public static ITODOListApplicationService GetTodoAppService
        {
            get 
            {
                return _todoAppService;
            }
        }

        public static ITaskService GetTaskServiceInstance
        {
            get
            {
                return _taskService;
            }            
        }

        public static ITaskRepository GetTaskRepository
        {
            get 
            {
                return _taskRepo;
            }
        }
    }
}
