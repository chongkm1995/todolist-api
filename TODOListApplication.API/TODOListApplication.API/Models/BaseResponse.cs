﻿namespace TODOListApplication.API.Models
{
    public class BaseResponse
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;
        public object Result { get; set; } = new object();
    }
}
